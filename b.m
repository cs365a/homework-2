close all;
clear all;
k = 6;
files = dir(fullfile('images', '*.jpg'));
sizeXY = [115 144];
data = [];
len = length(files);
for i = 1:length(files)
    temp = imread(strcat('images/',files(i).name));
    temp = imresize(temp, sizeXY);
    temp = rgb2gray(temp);
    temp = reshape(temp(:,:,1), [], 1);
    data = [data temp];
end
D = L2_distance(double(data),double(data));
options.dims = 1:10;
options.display = 1;
options.overlay = 1;
[Y, R, E] = Isomap(D, 'k', k, options);
[val, index] = min(R);
f5 = figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
axis normal;
plot(Y.coords{2}(1,:),Y.coords{2}(2,:), '.');
list = randperm(len, 10);
for i = list
    plot(Y.coords{2}(1, i),Y.coords{2}(2, i), '.r');
end
for i = list
    im = reshape(data(:, i), sizeXY);
    image(Y.coords{2}(1, i)+ 100 +[0, 144]*20, Y.coords{2}(2, i) +[115, 0]*20, cat(3, im, im, im));
    gplot(ones(2,2), [Y.coords{2}(1, i),Y.coords{2}(2, i);Y.coords{2}(1, i)+100, Y.coords{2}(2, i)] );
    text(Y.coords{2}(1, i)-50, Y.coords{2}(2, i)+30, strcat('image-', int2str(i)));
end
saveas(f5, 'partBrandom.png');
hold off;
f1 = figure;
hold on;
plot(Y.coords{2}(1,:),Y.coords{2}(2,:), '.');
distance = L2_distance(Y.coords{index}, Y.coords{index});
distance = distance(25, :);
[m, mi] = sort(distance);
least7indices = mi(1:(k+1));
plot(Y.coords{2}(1,25),Y.coords{2}(2,25), '.g');
for i = least7indices(:, 2:end);
    plot(Y.coords{2}(1,i),Y.coords{2}(2,i), '.r');
end
legend1 = legend('Points not in neighbourhood', 'Image 25', 'Images in neighbourhood', 'Location','northoutside','Orientation','horizontal');
set(legend1,'FontSize',9);
saveas(f1, 'partBnn25.png');
hold off;
f2 = figure;
hold on;
axis equal;
for i = least7indices
    im = reshape(data(:, i), sizeXY);
    plot(Y.coords{2}(1, i),Y.coords{2}(2, i), 'ro');
    image(Y.coords{2}(1, i) +[0, 144]*10,  Y.coords{2}(2, i)+ 500 +[115, 0]*10, cat(3, im, im, im));
    gplot(ones(2,2), [Y.coords{2}(1, i),Y.coords{2}(2, i);Y.coords{2}(1, i), Y.coords{2}(2, i)+ 500] );
    text(Y.coords{2}(1, i), Y.coords{2}(2, i)-50, strcat('image-', int2str(i)));
end
saveas(f2, 'partBwithimage25.png');
hold off;
f3 = figure;
hold on;
plot(Y.coords{2}(1,:),Y.coords{2}(2,:), '.');
distance = L2_distance(Y.coords{2}, Y.coords{2});
distance = distance(10, :);
[m, mi] = sort(distance);
least7indices = mi(1:(k+1));
adj = zeros(length(files), length(files));
plot(Y.coords{2}(1,10),Y.coords{2}(2,10), '.g');
for i = least7indices(:, 2:end);
    plot(Y.coords{2}(1,i),Y.coords{2}(2,i), '.r');
end
legend1 = legend('Points not in neighbourhood', 'Image 10', 'Images in neighbourhood', 'Location','northoutside','Orientation','horizontal');
set(legend1,'FontSize',9);
saveas(f3, 'partBnn10.png');
hold off;
f4 = figure;
axis equal;
hold on;
for i = least7indices
    im = reshape(data(:, i), sizeXY);
    plot(Y.coords{2}(1, i),Y.coords{2}(2, i), 'ro');
    image(Y.coords{2}(1, i)+ 100 +[0, 144], Y.coords{2}(2, i) +[115, 0], cat(3, im, im, im));
    gplot(ones(2,2), [Y.coords{2}(1, i),Y.coords{2}(2, i);Y.coords{2}(1, i)+100, Y.coords{2}(2, i)] );
    text(Y.coords{2}(1, i)-50, Y.coords{2}(2, i)+30, strcat('image-', int2str(i)));
end
hold off;
saveas(f4, 'partBwithimage10.png');
