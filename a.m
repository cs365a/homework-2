close all;
clear all;
files = dir(fullfile('images/', '*.jpg'));
data = [];
sizeXY = [115 144];
len = length(files);
for i = 1:length(files)
    temp = imread(strcat('images/',files(i).name));
    temp = imresize(temp, sizeXY);
    temp = rgb2gray(temp);
    temp = reshape(temp(:,:,1), [], 1);
    data = [data temp];
end
u = mean(data,2);
X = double(data) - u*ones(1, len);
C = (X'*X)/(len-1);
[V, D] = eig(C);
eigens = normc(X*V);
xx = [1, 2, 10, 30, 80, len];
imwrite(reshape(mat2gray(data(:, 20)) ,sizeXY(1), sizeXY(2)) , 'originalImage10.jpg');
for i = xx
    reduced = eigens(:, len-i+1:len)'*X;
    reconstructed = eigens(:, len-i+1:len)*reduced + u*ones(1, len);
    figure;
    imshow(reshape(mat2gray(reconstructed(:, 20)), sizeXY(1), sizeXY(2)));
    imwrite(reshape(mat2gray(reconstructed(:, 20)) ,sizeXY(1), sizeXY(2)) , strcat('image', int2str(i), '.jpg'));
    hold off;
end;
