close all;
clear all;
files = dir(fullfile('images/', '*.jpg'));
data = [];
sizeXY = [115 144];
len = length(files);
for i = 1:length(files)
    temp = imread(strcat('images/',files(i).name));
    temp = imresize(temp, sizeXY);
    temp = rgb2gray(temp);
    temp = reshape(temp(:,:,1), [], 1);
    data = [data temp];
end
params = {'linear', 'poly', 'gauss'};
for j = params
    map = compute_mapping(data', 'KernelPCA', 2, j{1});
    figure;
    hold on;
    h = plot(map(:, 1), map(:, 2), '.');
    title(strcat('2-D embedding using', {' '}, j{1}, ' kernel'));
    saveas(h, strcat('kernelPCA_', j{1}, '.png'));
    hold off;
    f1 = figure();
    hold on;
    axis normal;
    plot(map(:,1),map(:, 2), '.');
    list = randperm(len, 5);
    for i = list
        plot(map(i, 1),map(i, 2), '.r');
    end
    scaleY = 1;
    switch j{1}
        case 'linear'
            scale = 10;
        case 'poly'
            scale = 2e+9;
        case 'gauss'
            scale = 0.1;
            scaleY = 0.01;
    end
    for i = list
        im = reshape(data(:, i), sizeXY);
        image(map(i, 1)+ 100*scaleY +[0, 144]*scaleY*scale, map(i, 2) +[115, 0]*scale*scaleY, cat(3, im, im, im));
        gplot(ones(2,2), [map(i, 1),map(i, 2);map(i, 1)+100*scaleY, map(i, 2)] );
        text(map(i, 1), map(i, 2), strcat('image-', int2str(i)));
    end
    saveas(f1, strcat('partCrandom_', j{1}, '.png'));
    hold off;
end
